package linters

import "io"

// Linter represents the information required to register a new linter
type Linter interface {
	Name() string
	URL() string
	CmdLineArgs(args []string) []string
	OutStream() io.Reader

	Run(w io.Writer, r io.Reader) error
}
