package hadolint

import (
	"io"
	"os"

	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/linters"
)

type linter struct {
}

func New() linters.Linter {
	return new(linter)
}

func (l *linter) Name() string {
	return "hadolint"
}

func (l *linter) URL() string {
	return "https://github.com/hadolint/hadolint#install"
}

func (l *linter) CmdLineArgs(args []string) []string {
	return append(args, "--format", "codeclimate")
}

func (l *linter) OutStream() io.Reader {
	return os.Stdout
}

func (l *linter) Run(w io.Writer, r io.Reader) error {
	_, err := io.Copy(w, r)
	return err
}
