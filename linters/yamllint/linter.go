package yamllint

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"

	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/codeclimate"
	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/linters"
)

type linter struct {
	re *regexp.Regexp
}

func New() linters.Linter {
	return &linter{
		re: regexp.MustCompile(
			`(?U)^(?P<file>.*):(?P<line>\d+):\d+: \[(?P<severity>\w+)\] (?P<description>.*?) \((?P<checkname>.*)\)$`),
	}
}

func (l *linter) Name() string {
	return "yamllint"
}

func (l *linter) URL() string {
	return "https://github.com/adrienverge/yamllint#installation"
}

func (l *linter) CmdLineArgs(args []string) []string {
	return append(args, "-f", "parsable")
}

func (l *linter) OutStream() io.Reader {
	return os.Stdout
}

func (l *linter) Run(w io.Writer, r io.Reader) error {
	enc := json.NewEncoder(w)

	// Convert to Code Climate entries
	s := bufio.NewScanner(r)
	for s.Scan() {
		line := s.Text()

		matches := l.re.FindStringSubmatch(line)
		if len(matches) != 6 {
			return fmt.Errorf("reading yamllint output: %s", line)
		}

		lineNumber, err := strconv.Atoi(matches[2])
		if err != nil {
			return fmt.Errorf("converting yamllint line number: %w", err)
		}

		ccIssue := codeclimate.Issue{
			Type:        "issue",
			Description: matches[4],
			CheckName:   matches[5],
			Location: codeclimate.Loc{
				Path: matches[1],
				Lines: codeclimate.Lines{
					Begin: lineNumber,
					End:   lineNumber,
				},
			},
			Severity: getSeverity(matches[3]),
		}

		// Output Code Climate JSON
		err = enc.Encode(&ccIssue)
		if err != nil {
			return fmt.Errorf("marshalling Code Climate output: %w", err)
		}
	}

	return s.Err()
}

func getSeverity(level string) string {
	switch level {
	case "error":
		return "critical"
	case "warning":
		return "major"
	}

	return ""
}
