package linters

import (
	"errors"
)

// Manager holds all the supported linters
type Manager interface {
	Register(linter Linter) error

	Linters() []Linter
	Linter(name string) Linter
}

type manager struct {
	linters []Linter
}

func New() Manager {
	return new(manager)
}

func (m *manager) Register(linter Linter) error {
	if linter.Name() == "" {
		return errors.New("linter name cannot be empty")
	}

	m.linters = append(m.linters, linter)

	return nil
}

func (m *manager) Linters() []Linter {
	return m.linters
}

func (m *manager) Linter(name string) Linter {
	for _, linter := range m.linters {
		if name == linter.Name() {
			return linter
		}
	}
	return nil
}
