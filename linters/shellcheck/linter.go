package shellcheck

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/codeclimate"
	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/linters"
)

type linter struct {
}

type shellCheckResult struct {
	File      string `json:"file"`
	Line      int    `json:"line"`
	EndLine   int    `json:"endLine"`
	Column    int    `json:"column"`
	EndColumn int    `json:"endColumn"`
	Level     string `json:"level"`
	Code      int    `json:"code"`
	Message   string `json:"message"`
}

func New() linters.Linter {
	return new(linter)
}

func (l *linter) Name() string {
	return "shellcheck"
}

func (l *linter) URL() string {
	return "https://github.com/koalaman/shellcheck#installing"
}

func (l *linter) CmdLineArgs(args []string) []string {
	return append(args, "-f", "json")
}

func (l *linter) OutStream() io.Reader {
	return os.Stdout
}

func (l *linter) Run(w io.Writer, r io.Reader) error {
	enc := json.NewEncoder(w)
	dec := json.NewDecoder(r)

	// Convert to Code Climate entries
	var results []shellCheckResult
	err := dec.Decode(&results)
	if err == io.EOF {
		return nil
	}
	if err != nil {
		return fmt.Errorf("unmarshalling shellcheck output: %w", err)
	}

	// Parse linter stdout JSON output
	for _, result := range results {
		// Convert to Code Climate issues
		ccIssue := codeclimate.Issue{
			Type:        "issue",
			Description: result.Message,
			CheckName:   fmt.Sprintf("SC%d", result.Code),
			Location: codeclimate.Loc{
				Path: result.File,
				Lines: codeclimate.Lines{
					Begin: result.Line,
					End:   result.EndLine,
				},
			},
			Severity: getSeverity(result.Level),
		}

		// Output Code Climate JSON
		err = enc.Encode(&ccIssue)
		if err != nil {
			return fmt.Errorf("marshalling Code Climate output: %w", err)
		}
	}

	return nil
}

func getSeverity(level string) string {
	switch level {
	case "error":
		return "critical"
	case "warning":
		return "major"
	case "info":
		return "info"
	case "style":
	case "verbose":
	case "message":
	case "source":
	}

	return ""
}
