package markdownlint

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/codeclimate"
	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/linters"
)

type linter struct {
	re *regexp.Regexp
}

func New() linters.Linter {
	return &linter{
		re: regexp.MustCompile(
			`(?U)^(?P<file>.*):(?P<line>\d+)(:\d+)? (?P<checkname>\S+) (?P<description>.*)$`),
	}
}

func (l *linter) Name() string {
	return "markdownlint"
}

func (l *linter) URL() string {
	return "https://github.com/igorshubovych/markdownlint-cli#installation"
}

func (l *linter) CmdLineArgs(args []string) []string {
	return args
}

func (l *linter) OutStream() io.Reader {
	return os.Stderr
}

func (l *linter) Run(w io.Writer, r io.Reader) error {
	enc := json.NewEncoder(w)

	// Convert to Code Climate entries
	s := bufio.NewScanner(r)
	read := 0
	for s.Scan() {
		line := s.Text()

		if read == 0 && line == "" {
			// If the first line of output is empty, that usually indicates that markdownlint is outputting an error
			// to the stream. Let's parse it and return an error to the caller
			return l.parseLinterError(s)
		}
		read += len(line)

		ccIssue, err := l.parseEntry(line)
		if err != nil {
			return err
		}

		// Output Code Climate JSON
		err = enc.Encode(&ccIssue)
		if err != nil {
			return fmt.Errorf("marshalling Code Climate output: %w", err)
		}
	}

	return s.Err()
}

func (l *linter) parseEntry(line string) (*codeclimate.Issue, error) {
	matches := l.re.FindStringSubmatch(line)
	if len(matches) != 6 {
		return nil, fmt.Errorf("reading markdownlint output: %s", line)
	}

	// Parse line number and skip column number
	lineNumber, err := strconv.Atoi(matches[2])
	if err != nil {
		return nil, fmt.Errorf("converting markdownlint line number: %w", err)
	}

	ccIssue := &codeclimate.Issue{
		Type:        "issue",
		Description: matches[5],
		CheckName:   matches[4],
		Location: codeclimate.Loc{
			Path: matches[1],
			Lines: codeclimate.Lines{
				Begin: lineNumber,
				End:   lineNumber,
			},
		},
	}
	return ccIssue, nil
}

func (l *linter) parseLinterError(s *bufio.Scanner) error {
	sb := new(strings.Builder)
	for s.Scan() {
		line := s.Text()
		sb.WriteString(line)
		sb.WriteByte('\n')
	}

	err := s.Err()
	if err == io.EOF {
		return fmt.Errorf("reading markdownlint output: %s", strings.TrimSpace(sb.String()))
	}

	return fmt.Errorf("reading markdownlint output: %w", err)
}
