package codeclimate

// Implement structures for the Code Climate spec
// See https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md

// Lines represents the JSON layout of a CC line range that will be output by linter2cc
type Lines struct {
	Begin int `json:"begin"`
	End   int `json:"end,omitempty"`
}

// Loc represents the JSON layout of a CC location that will be output by linter2cc
type Loc struct {
	Path  string `json:"path"`
	Lines Lines  `json:"lines"`
}

// Content represents the JSON layout of a CC content field that will be output by linter2cc
type Content struct {
	Body string `json:"body"`
}

// Issue represents the JSON layout of an issue that will be output by linter2cc
type Issue struct {
	Type              string   `json:"type"`
	CheckName         string   `json:"check_name"`
	Description       string   `json:"description"`
	Content           *Content `json:"content,omitempty"`
	Location          Loc      `json:"location"`
	OtherLocations    []Loc    `json:"other_locations,omitempty"`
	RemediationPoints int      `json:"remediation_points,omitempty"`
	Severity          string   `json:"severity,omitempty"`
	Categories        []string `json:"categories,omitempty"`
	Fingerprint       string   `json:"fingerprint,omitempty"`
}
